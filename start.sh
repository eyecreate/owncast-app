#!/bin/bash

set -eu

# ensure that data directory is owned by 'cloudron' user
chown -R cloudron:cloudron /app/data

echo "Starting Owncast app"

# run the app as user 'cloudron'
exec /usr/local/bin/gosu cloudron:cloudron /app/code/owncast -configFile /app/data/config.yaml -chatDatabase /app/data/chat.db
 
