FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4

EXPOSE 8080 1935

RUN apt update && apt install -y ffmpeg
RUN mkdir -p /app/code
RUN mkdir -p /app/data
WORKDIR /app/code
ADD start.sh /app/code

RUN wget https://github.com/owncast/owncast/releases/download/v0.0.2/owncast-linux-0.0.2.zip && unzip owncast-linux-0.0.2.zip && rm owncast-linux-0.0.2.zip
RUN cp /app/code/config.yaml /app/data/config.yaml
RUN chmod +x /app/code/start.sh
RUN ln -s /app/data/stats.json /app/code/stats.json
RUN mkdir -p /app/data/hls
RUN ln -s /app/data/hls /app/code/hls

CMD [ "/app/code/start.sh" ]
